<div id="cadre_reinit">
    <form action="index.php?admin=user" method="post">
      <table>
        <tr>
          <td><p class="message_reinit">Login utilisateur * :</p></td>
          <td><input type="text"  id="login" name="login" value="{{login}}" required="required"></input></td>
        </tr>
        <tr>
          <td><p class="message_reinit">Description utilisateur * :</p></td>
          <td><input type="text" class="input_connexion" id="description" name="description" value="{{description}}" required="required"></input></td>
        </tr>
        <tr>
          <td><p class="message_reinit">Mail utilisateur * :</p></td>
          <td><input type="text" class="input_connexion" id="mail" name="mail" value="{{mail}}" required="required"></input></td>
        </tr>
        <tr>
          <td><p class="message_reinit">Profil utilisateur * :</p></td>
          <td>
            <select name="profil" id="profil">
                <option {% if profil =='' %} selected="selected" {% endif %} value=''></option>
                <option {% if profil =='3' %} selected="selected" {% endif %} value='3'>Administrateur</option>
                <option {% if profil =='4' %} selected="selected" {% endif %} value='4'>Métier</option>
                <option {% if profil =='1' %} selected="selected" {% endif %} value='1'>Notaire</option>
            </select>
          </td>
        </tr>
        <tr>
          <td></td>
          <td align="center"><input type="submit" id="enregistrer" name="enregistrer" value="Enregistrer"></input></td>
        </tr>
      </table>
    </form>
</div>