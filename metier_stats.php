﻿<?php


if(isset($_SESSION['profileNotaire']) && ($_SESSION['profileNotaire']==3 || $_SESSION['profileNotaire']==4)){


$_SESSION['stats_year_endNotaire']="";
$_SESSION['stats_month_endNotaire']="";
$_SESSION['stats_year_startNotaire']="";
$_SESSION['stats_month_startNotaire']="";

if(isset($_POST['submit_listing'])){
	if (isset($_POST['year_end']))
	$_SESSION['stats_year_endNotaire']=$_POST['year_end'];
	
	if (isset($_POST['month_end']))
	$_SESSION['stats_month_endNotaire']=$_POST['month_end'];
	
	if (isset($_POST['year_start']))
	$_SESSION['stats_year_startNotaire']=$_POST['year_start'];
	
	if (isset($_POST['month_start']))
	$_SESSION['stats_month_startNotaire']=$_POST['month_start'];
?>
<script>
		window.open('stats.php','_blank');
</script>
<?php
	
}


function statistiques_log()
 {
	$mois = date('n');
	$annee = date('y');
	
	// Variable mensuelles en texte (Mois_Moins_Un, _Deux, _Trois, _Quatre)
	$mois_actu = utf8_encode(strftime("%B-%y ", mktime(0, 0, 0, $mois+1, 0, $annee)));
	$mois_m_u = utf8_encode(strftime("%B-%y ", mktime(0, 0, 0, $mois, 0, $annee)));
	$mois_m_d = utf8_encode(strftime("%B-%y ", mktime(0, 0, 0, $mois-1, 0, $annee)));
	$mois_m_t = utf8_encode(strftime("%B-%y ", mktime(0, 0, 0, $mois-2, 0, $annee)));
	$mois_m_q = utf8_encode(strftime("%B-%y ", mktime(0, 0, 0,$mois-3, 0, $annee)));

	//Variables mensuelles en chiffre (Mois_Moins_Un_Numérique, _Deux_Numérique, _Trois_Numérique, _Quatre_Numérique)
	$mactun = date("n");
	$annee = date('Y');
	if ($mactun == 1)
	{
		$mmun= 12; $amun=$annee-1;
		$mmdn= 11; $amdn=$annee-1;
		$mmtn = 10; $amtn=$annee-1;
		$mmqn = 9; $amqn=$annee-1;
	}
	elseif ($mactun == 2)
	{
		$mmun= 1; $amun=$annee;
		$mmdn= 12; $amdn=$annee-1;
		$mmtn = 11; $amtn=$annee-1;
		$mmqn = 10;	$amqn=$annee-1;
	}
	elseif ($mactun == 3)
	{
		$mmun= 2; $amun=$annee;
		$mmdn= 1; $amdn=$annee;
		$mmtn = 12; $amtn=$annee-1;
		$mmqn = 11;	$amqn=$annee-1;
	}
		elseif ($mactun == 4)
	{
		$mmun= 3; $amun=$annee;
		$mmdn= 2; $amdn=$annee;
		$mmtn = 1; $amtn=$annee;
		$mmqn = 12;	$amqn=$annee-1;
	}
	else
	{
		$mmun= date("n") -1; $amun=$annee;
		$mmdn= date("n")-2; $amdn=$annee;
		$mmtn = date("n")-3; $amtn=$annee;
		$mmqn = date("n")-4; $amqn=$annee;
	}

	echo '<table class=table_stats width=100%>';
	echo '<tr class=stats><th></th><th width=150><b>'.$mois_actu .'</b></th><th width=150>'.$mois_m_u.'</th>
	<th width=150>'.$mois_m_d.'</th><th width=150>'.$mois_m_t.'</th><th width=150>'.$mois_m_q.'</th></tr>';
	echo '<tr class=stats><td>Nombre de demandes</td><td align=center><b>'.staque($mactun,$annee).'</b></td>
	<td align=center>'.staque($mmun,$amun).'</td><td align=center>'.staque($mmdn,$amdn).'</td><td align=center>'
	.staque($mmtn,$amtn).'</td><td align=center>'.staque($mmqn,$amqn).'</td></tr>';
	echo '<tr class=stats><td>Nombre de réponses : </td><td align=center><b>'.stats_rep($mactun,$annee).'</b></td>
	<td align=center>'.stats_rep($mmun,$amun).'</td><td align=center>'.stats_rep($mmdn,$amdn).'</td><td align=center>'
	.stats_rep($mmtn,$amtn).'</td><td align=center>'.stats_rep($mmqn,$amqn).'</td></tr>';
	echo '<tr class=stats><td>  -  Récupèrables</td><td align=center><b>'.starepnat($mactun ,"1",$annee).'</b></td>
	<td align=center>'.starepnat($mmun ,"1",$amun).'</td><td align=center>'.starepnat($mmdn,"1",$amdn).'</td>
	<td align=center>'.starepnat($mmtn,"1",$amtn).'</td><td align=center>'.starepnat($mmqn,"1",$amqn).'</td></tr>';
	echo '<tr class=stats><td>  -  Indus Probables</td><td align=center><b>'.starepnat($mactun,"2",$annee).'</b></td>
	<td align=center>'.starepnat($mmun ,"2",$amun).'</td><td align=center>'.starepnat($mmdn,"2",$amdn).'</td>
	<td align=center>'.starepnat($mmtn,"2",$amtn).'</td><td align=center>'.starepnat($mmqn,"2",$amqn).'</td></tr>';
	echo '<tr class=stats><td>  -  Cas ambigus</td><td align=center><b>'.starepnat($mactun,"4",$annee).'</b></td>
	<td align=center>'.starepnat($mmun ,"4",$amun).'</td><td align=center>'.starepnat($mmdn,"4",$amdn).'</td>
	<td align=center>'.starepnat($mmtn,"4",$amtn).'</td><td align=center>'.starepnat($mmqn,"4",$amqn).'</td></tr>';
	echo '<tr class=stats><td>  -  Individus Inconnus</td><td align=center><b>'.starepnat($mactun,"3",$annee).'</b></td>
	<td align=center>'.starepnat($mmun ,"3",$amun).'</td><td align=center>'.starepnat($mmdn,"3",$amdn).'</td>
	<td align=center>'.starepnat($mmtn,"3",$amtn).'</td><td align=center>'.starepnat($mmqn,"3",$amqn).'</td></tr>';
	echo '<tr class=stats><td>Nombre de connexions</td><td align=center><b>'.status($mactun,$annee).'</b></td>
	<td align=center>'.status($mmun,$amun).'</td><td align=center>'.status($mmdn,$amdn).'</td><td align=center>'
	.status($mmtn,$amtn).'</td><td align=center>'.status($mmqn,$amqn).'</td></tr>';
	echo "<tr class=stats><td>Nombre d'utilisateurs différents</td><td align=center><b>".staudif($mactun,$annee).'</b></td>
	<td align=center>'.staudif($mmun,$amun).'</td><td align=center>'.staudif($mmdn,$amdn).'</td><td align=center>'
	.staudif($mmtn,$amtn).'</td><td align=center>'.staudif($mmqn,$amqn).'</td></tr>';
	//echo '<tr><td align=center>Nombre moyen de recherche par utilisateur</td><td align=center>'.stats_user($mactun)/stats_user_dif($mactun).'</td><td></td><td></td><td></td><td></td></tr>'; 
	echo '</table>';

}


$req_1="SELECT distinct year(date) FROM `log_membre` order by year(date) ASC;";
$res = $connect->query($req_1);
$annee_1=array();
$mois_1=array();
while($var = $res->fetch()){
	$annee_1[]=$var[0];
}

$annee_2=array_unique($annee_1);

?>

<div id=recherche_listing>
	<form name="selecto" method=post action="index.php?metier=stats" onsubmit="return compare_Date('','month_start','year_start','','month_end','year_end');">
		<div id=listing_date_deb>
			<label  id=listing for=day_start>Date de début : </label>
			  <select id="year_start" name="year_start" onchange="Afficher_mois_Stats('year_start','month_start')"/>  
            <option value=""></option>
            <?php
              foreach($annee_2 as $add){
              ?>
                 <option value='<?php echo $add;?>' <?php if(isset($_SESSION['stats_year_startNotaire']) && 
                    $_SESSION['stats_year_startNotaire']==$add){echo "selected";}?> ><?php echo $add;?></option>
              <?php
              }
              ?>    
        </select>
        <select id="month_start" name="month_start" />
			<?php
			if(isset($_SESSION['stats_month_startNotaire'])){ 
				$req_1="SELECT distinct month(date) FROM `log_membre` where year(date)='".$_SESSION['stats_year_startNotaire']."' order by month(date) ASC;";
				$res = $connect->query($req_1);
				while($var = $res->fetch()){
			?>
				<option value='<?php echo $var[0]; ?>' <?php if(isset($_SESSION['stats_month_startNotaire']) && 
                    $_SESSION['stats_month_startNotaire']==$add){echo "selected";}?> ><?php echo $mois[$var[0]]; ?></option>
			<?php
				}
			}
			?>
		</select> 
			  
		<label id=listing for=day_end>Date de fin : </label>
		<select id="year_end" name="year_end" onchange="Afficher_mois_Stats('year_end','month_end')"/>
            <option value=""></option>
            <?php
              foreach($annee_2 as $add){
              ?>
                 <option value='<?php echo $add;?>' <?php if(isset($_SESSION['stats_year_endNotaire']) && 
                    $_SESSION['stats_year_endNotaire']==$add){echo "selected";}?> ><?php echo $add;?></option>
              <?php
              }
              ?>    
         </select>
              
        <select id="month_end" name="month_end" /> 
			<?php
			if(isset($_SESSION['stats_month_endNotaire'])){ 
				$req_1="SELECT distinct month(date) FROM `log_membre` where year(date)='".$_SESSION['stats_year_endNotaire']."' order by month(date) ASC;";
				$res = $connect->query($req_1);
				while($var = $res->fetch()){
			?>
				<option value='<?php echo $var[0]; ?>' <?php if(isset($_SESSION['stats_month_endNotaire']) && 
                    $_SESSION['stats_month_endNotaire']==$add){echo "selected";}?> ><?php echo $mois[$var[0]]; ?></option>
			<?php
				}
			}
			?>
		</select>
			  
			  <input type=submit id=submit_listing name=submit_listing value='Générer un PDF'></input>
		</div>
	</form>
</div>
<div id=stats>

<?php 
statistiques_log();
?>

<img src="stats_graphique.php"/>
</div>


<?php }
else{
	header ('Location: index.php');
}
?>