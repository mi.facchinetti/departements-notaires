# Installation de l'application

Sont décrites ici les étapes pour configurer et tester rapidement l'application.

## Pré-requis

* Apache 2.2 ou plus est installé : vous savez modifier si besoin la config dans `httpd.conf`, et arrêter/relancer
* PHP 5.6 ou plus est installé : vous savez modifier si besoin la config dans `php.ini`. Extensions requises :
   * pdo_mysql
   * imap 
   * mbstring
   * xml 
   * gd
* MySQL 5.7 ou plus est installé : vous connaissez les bases de l'administration, créer un utilisateur et accorder les droits

Il est supposé pour la suite que Apache est accessible via l'URL `http://localhost`, à adapter selon votre configuration
réelle.

## Récupérer l'application 

* Télécharger le fichier zip contenant la version en cours : [Télécharger](https://gitlab.adullact.net/departements-notaires/departements-notaires/repository/archive.zip?ref=master)
* Dézipper le fichier et recopier son contenu dans le sous-dossier `www/notaires` du serveur Apache

Certains répertoires présents dans l'arborescence ne doivent par être servis par Apache (les directives RewriteRule l'interdisent).
Il s'agit des répertoires :
* `.private` : graphique pour personnaliser les PDF générés
* `cron` : contient le log d'import quotidien, voir `cron/cron.log`
* `documentation` : comme son nom l'indique
* `lib` : des libs PHP internes au projet
* `documentation` : comme son nom l'indique
* `pdf` : dossiers où sont générés temporairement les fichiers PDF. Les fichiers sont supprimés après envoi par email
* `test` : contient des données et outils pour les tests
* `tpl` : contient les fichiers *.tpl pour personnaliser le contenu des lettres et emails (syntaxe Twig)
* `vendor` : contient des libs PHP fournies par des tiers


## Créer la base de données

Editer le fichier `documentation/install/notaires.sql` afin de remplacer **admin@xxx.fr** par l'email valide
de l'administrateur de l'application.

En tant que root de MySQL :
* Exécuter ensuite `notaires.sql` pour créer la base `notaires` et les tables associées. 
Un utilisateur `admin` est créé par défaut avec le mot de passe `admin_notaires`.
* Créer un utilisateur MySQL `notaires`, mot de passe `notaires` ayant tous les droits sur la base `notaires`.

## Configurer à minima

Editer `config.php`: 
* Modifier les paramètres d'accès à la base de données : `$host_mysql`, `$user_mysql`, `$pass_mysql`, `$bdd_mysql`
* Modifier les paramètres pour l'envoi des emails : `$smtp_mail`, `$port_smtp_mail`, `$mail_gestion`, `$password_mail`

Editer `htaccess.php`: 
* Modifier la ligne ou ce trouve ErrorDocument 403 pour mettre l'adresse du site
* Modifier la ligne ou ce trouve ErrorDocument 404 pour mettre l'adresse du site

## Créer des données de test

Editer `htaccess.php`: 
* Commenter la ligne RewriteRule ^test/ - [F] (ajouter un # devant)

Un échantillon de données (non réelles) est fourni pour tester l'application. Il se trouve dans le dossier `test`
dans les fichiers `demande.csv` et `individus.csv`.

Afin d'importer ces fichiers, lancer à la ligne de commande depuis le dossier `test`:

``` shell
php imp_data_test.php
```

## Utiliser l'application

* Ouvrir la page d'accueil [](http://localhost/notaires) et se connecter en utilisant les identifiants `admin`/`admin_notaires`.
* Réaliser une recherche ne donnant pas de réponse
* Vérifier que la boîte de réception correspondant à l'email de l'administrateur contient un nouvel email en réponse à la recherche



 
