## L'exportation des données sources issues des logiciels métiers

L'exportation des données de production issues du logiciel métier doit être réalisée de manière quotidienne (à j-1). 

Les données attendues de cette exportation sont décrites sous https://gitlab.adullact.net/departements-notaires/departements-notaires/blob/master/documentation/install/stucture_bdd.md

Cette extraction est propre à chaque Département en fonction de son logiciel métier et du paramétrage lié ou encore de son système d’extraction de données. 

À titre d’exemple : 
- vous trouverez ici la requête php Iodas utilisée pour le département du Rhone : https://gitlab.adullact.net/departements-notaires/departements-notaires/blob/master/cron/extract.php
- et ici les éléments de la requête utilisée par le département de l’Isère pour l’extraction Solis via un univers BO : https://gitlab.adullact.net/departements-notaires/departements-notaires/blob/master/documentation/import-solis.md
